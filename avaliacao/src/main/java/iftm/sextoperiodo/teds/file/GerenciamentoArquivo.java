package iftm.sextoperiodo.teds.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GerenciamentoArquivo {

	private String path;
	
	public GerenciamentoArquivo(String path) {
		this.path = path;
	}

	public void writeFile(String json) throws IOException {

		try {

			BufferedWriter bw = new BufferedWriter(new FileWriter(path, true));
			
			bw.write(json);
			bw.newLine();

			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<String> readFile() throws IOException {

		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new FileReader(path));
			List<String> data = new ArrayList<String>();
			String line = null;

			while ((line = br.readLine()) != null) {
				data.add(line);
			}

			return data;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		} finally {
			if (br != null) {
				br.close();
			}
		}

		return null;

	}

}
